Name:           makeself
Version:        2.5.0
Release:        1
BuildArch:      noarch
Summary:        Make self-extractable archives on Unix

License:        GPLv2+
URL:            https://github.com/megastep/makeself
Source:         https://github.com/megastep/%{name}/archive/release-%{version}/%{name}-%{version}.tar.gz

Patch0:         move_header.patch

BuildRequires:  glibc

Requires:       gzip

Recommends:     gnupg
Recommends:     openssl

Suggests:       bzip2
Suggests:       gzip
Suggests:       lz4
Suggests:       pigz
Suggests:       xz
Suggests:       zstd

%description
makeself.sh is a shell script that generates a self-extractable
tar.gz archive from a directory. The resulting file appears as a shell
script, and can be launched as is. The archive will then uncompress
itself to a temporary directory and an arbitrary command will be
executed (for example an installation script). This is pretty similar
to archives generated with WinZip Self-Extractor in the Windows world.
%prep
%setup -q -n %{name}-release-%{version}
%patch0

%build
iconv --from-code=ISO-8859-1 --to-code=UTF-8 %{name}.1 | gzip > %{name}.1.gz

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libexecdir}
mkdir -p %{buildroot}%{_mandir}/man1

install -p -m755 %{name}.sh %{buildroot}%{_bindir}
install -p -m644 %{name}-header.sh %{buildroot}%{_libexecdir}
install -p -m644 %{name}.1.gz %{buildroot}%{_mandir}/man1
ln -s %{name}.sh %{buildroot}%{_bindir}/%{name}

%files
%doc README.md COPYING
%{_mandir}/man1/*
%{_libexecdir}/*
%{_bindir}/*

%changelog
* Tue Dec 10 2024 liyunqing <liyunqing@kylinos.cn> - 2.5.0-1
- update to version 2.5.0
  - Adds improved support for NetBSD, OpenBSD
  - Improved support for minimal Linux distributions such as Alpine
  - Added bzip3 compression support
  - Increased control over GPG signing via the GPG extra parameters
  - Fix some bugs

* Wed Sep 01 2021 shenhongyi <shenhongyi2@huawei.com> - 2.4.2-1
- Package init
